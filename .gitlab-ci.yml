include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

variables:
  BUILD_BASE_SERIES: "focal"
  STAGING_IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-"
  IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/buildpack-deps:"
  IMAGE_VERSION: "1.0"
  REG_SHA256: "ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228"
  REG_VERSION: "v0.16.1"
  # FORCE_DEPLOY: 'yes'

  QEMU_NATIVE_ARCHES: '
    amd64-i386 i386-amd64
    arm-armel armel-arm arm-armhf armhf-arm armel-armhf armhf-armel
    powerpc-ppc64 ppc64-powerpc
    sparc-sparc64 sparc64-sparc
    s390-s390x s390x-s390
    '

stages:
  - prerequisites
  - build
  - deploy
  - cleanup

updated Dockerfiles:
  stage: prerequisites
  image: ubuntu
  variables:
    DEBIAN_FRONTEND: 'noninteractive'
  before_script:
    - apt-get update --quiet
    - apt-get install --no-install-recommends --yes
          ca-certificates gawk git jq wget
  script:
    - ./update.sh
    - test -z "$(git status -s)"

.build-template: &build-template
  stage: build
  image: vicamo/docker:$BUILD_BASE_SERIES-amd64-git
  services:
    - docker:dind
  variables:
    DEBIAN_FRONTEND: 'noninteractive'

    # JOB_QEMU_SUITE: string, default to ${JOB_CODENAME}
  before_script:
    - env; set -x

    - docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}

    - export JOB_DISTRO="$(echo "${CI_JOB_NAME}" | cut -d ':' -f2)"
    - export JOB_CODENAME="$(echo "${CI_JOB_NAME}" | cut -d ':' -f3)"
    - export JOB_ARCH="$(echo "${CI_JOB_NAME}" | cut -d ':' -f4)"
    - export JOB_DIR="${JOB_DISTRO}/${JOB_CODENAME}/${JOB_ARCH}"
    - export JOB_QEMU_SUITE=${JOB_QEMU_SUITE:-${JOB_CODENAME}}
    - export staging_image="${STAGING_IMAGE_PREFIX}${JOB_CODENAME}-${JOB_ARCH}"

    - host_arch="$(dpkg --print-architecture)"
    - if [[ ! " ${host_arch}-${host_arch} ${QEMU_NATIVE_ARCHES} " =~ "${host_arch}-${JOB_ARCH}" ]]; then
        docker run --rm --privileged vicamo/binfmt-qemu:${JOB_QEMU_SUITE};
      fi

    - apt-get update --quiet
    - apt-get install --no-install-recommends --yes jq

    - git clone https://github.com/docker-library/official-images.git
          "${CI_BUILDS_DIR}"/official-images
  script:
    - description=$(wget -q -O - "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq -r .description)
    - label_args=(
          --label org.opencontainers.image.title="${CI_PROJECT_TITLE}"
          --label org.opencontainers.image.description="${description}"
          --label org.opencontainers.image.authors="You-Sheng Yang"
          --label org.opencontainers.image.url="${CI_PROJECT_URL}"
          --label org.opencontainers.image.documentation="${CI_PROJECT_URL}"
          --label org.opencontainers.image.created="$(date +%Y-%m-%dT%H:%M:%SZ)"
          --label org.opencontainers.image.source="${CI_PROJECT_URL}.git"
          --label org.opencontainers.image.version="${IMAGE_VERSION}.${CI_PIPELINE_IID}"
          --label org.opencontainers.image.revision="${CI_COMMIT_SHA}"
          --label org.opencontainers.image.licenses="Docker"
      )

    - current_image=
    - for variant in curl scm ''; do
        if [ -n "${current_image}" ]; then
          base_image="${current_image}";
        elif [ "${JOB_DISTRO}" = "ubuntu" ]; then
          base_image="registry.gitlab.com/vicamo/docker-brew-ubuntu-core/ubuntu:${JOB_CODENAME}-${JOB_ARCH}";
        else
          base_image=;
        fi;
        current_image="${staging_image}${variant:+-${variant}}";
        docker build -t "${current_image}"
            ${base_image:+--build-arg "BASE_IMAGE=${base_image}"}
            --label org.opencontainers.image.ref.name="${IMAGE_PREFIX}${JOB_CODENAME}-${JOB_ARCH}${variant:+-${variant}}-${IMAGE_VERSION}"
            "${label_args[@]}"
            "${JOB_DIR}${variant:+/${variant}}";
      done

    - docker run --rm "${current_image}-curl" which curl
    - docker run --rm "${current_image}-scm" which git
    - docker run --rm "${current_image}" which gcc
    - ${CI_BUILDS_DIR}/official-images/test/run.sh "${current_image}"

    - skip_deploy=;
      published_image="${IMAGE_PREFIX}${JOB_CODENAME}-${JOB_ARCH}";
      if docker pull --quiet "${published_image}"; then
        published_version=$(docker inspect
          --format '{{index .Config.Labels "org.opencontainers.image.version"}}'
          "${published_image}");
        published_version="${published_version%.*}";
        if [ -z "${published_version}" ]; then
          true;
        elif [ "${published_version}" = "${IMAGE_VERSION}" ]; then
          docker run --rm "${staging_image}" dpkg-query -W > /tmp/current.manifest;
          docker run --rm "${published_image}" dpkg-query -W > /tmp/published.manifest;
          if diff -Nu /tmp/published.manifest /tmp/current.manifest 2>/dev/null; then
            skip_deploy=1;
          fi;
        else
          older_version="$({ echo "${IMAGE_VERSION}"; echo "${published_version}"; }
            | sort --version-sort | head -n 1)";
          if [ "${older_version}" = "${IMAGE_VERSION}" ]; then
            skip_deploy=1;
          fi;
        fi;
        docker rmi "${published_image}";
      fi
    - if [ -z "${skip_deploy}" ] || [ -n "${FORCE_DEPLOY}" ]; then
        for variant in curl scm ''; do
          current_image="${staging_image}${variant:+-${variant}}";
          docker push --quiet "${current_image}";
        done;
      fi
  after_script:
    - docker images

.retrieve-reg-tool: &retrieve-reg-tool |
  wget -O /usr/local/bin/reg "https://github.com/genuinetools/reg/releases/download/${REG_VERSION}/reg-linux-amd64"
  echo "${REG_SHA256}  /usr/local/bin/reg" | sha256sum -c -
  chmod a+x /usr/local/bin/reg

.deploy-template: &deploy-template
  stage: deploy
  image: docker:git
  services:
    - docker:dind
  before_script:
    - env; set -x

    - if [ "${CI_JOB_NAME%%:*}" = "deploy" ]; then
        export JOB_CODENAME="$(echo "${CI_JOB_NAME}" | cut -d ':' -f2)";
      fi

    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_JOB_TOKEN}" "${CI_REGISTRY}"
    - i=0;
      image_prefixes="${IMAGE_PREFIX}";
      while true; do
        var=EXTRA_DOCKER_SERVER_${i};
        eval "server=\${${var}:-}";
        var=EXTRA_DOCKER_USER_${i};
        eval "user=\${${var}:-}";
        var=EXTRA_DOCKER_PASSFILE_${i};
        eval "passfile=\${${var}:-}";
        var=EXTRA_DOCKER_REPO_${i};
        eval "repo=\${${var}:-}";
        [ -n "${user}" ] && [ -n "${passfile}" ] && [ -n "${repo}" ] || break;

        cat "${passfile}" | base64 -d |
          docker login --username "${user}" --password-stdin
              ${server:+"${server}"};
        image_prefixes="${image_prefixes} ${repo}:";

        i=$(( i + 1 ));
      done

    - *retrieve-reg-tool

  script:
    - staging_tags=$(
        /usr/local/bin/reg tags --auth-url "${CI_REGISTRY}"
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
            "${STAGING_IMAGE_PREFIX%:*}" |
          grep "^${STAGING_IMAGE_PREFIX##*:}${JOB_CODENAME}" || true)
    - for alias in oldoldstable oldstable stable testing unstable; do
        codename=$(wget -qO - http://deb.debian.org/debian/dists/${alias}/Release
          | awk '/^Codename:/ { print $2 }');
        eval "${alias}_codename=${codename}";
      done
    - eval "$(wget -qO - http://archive.ubuntu.com/ubuntu/dists/devel/Release
        | awk '/^Codename:/ { print "devel_codename=" $2 } /^Version:/ { print "devel_version=" $2 }')"
    - devel_version_year=${devel_version%.*}
    - devel_version_month=${devel_version#*.}
    - devel_version_norm=$((devel_version_year * 12 + devel_version_month))
    - test -z "${staging_tags}" || for staging_tag in ${staging_tags}; do
        codename=$(echo "${staging_tag}" | cut -d- -f2);
        arch=$(echo "${staging_tag}" | cut -d- -f3);
        variant=$(echo "${staging_tag}" | cut -d- -f4);
        distro=; for d in */${codename}; do distro=${d%/*}; break; done;

        [ -z "${variant}" ] || continue;

        aliases="${codename}";
        for alias in oldoldstable oldstable stable testing unstable devel; do
          eval "alias_codename=\${${alias}_codename}";
          if [ "${codename}" = "${alias_codename}" ]; then
            aliases="${aliases} ${alias}";
            if [ "${alias}" = "stable" ]; then
              aliases="${aliases} latest";
            fi;
          fi;
        done;
        if [ "${distro}" = "ubuntu" ]; then
          eval "version=$({
            wget -qO - http://archive.ubuntu.com/ubuntu/dists/${codename}/Release
              || wget -qO - http://old-releases.ubuntu.com/ubuntu/dists/${codename}/Release; }
            | awk '/^Version:/ {print $2}')";
          aliases="${aliases} ${version}";

          version_year=${version%.*};
          version_month=${version#*.};
          version_norm=$((version_year * 12 + version_month));
          if [ "$((devel_version_norm - version_norm))" = "6" ]; then
            aliases="${aliases} rolling";
          fi;
        fi;

        tags=;
        for alias in ${aliases}; do
          tags="${tags} ${alias}-${arch}";
          if [ "${arch}" = "amd64" ]; then
            tags="${tags} ${alias}";
          fi;
          if [ "${alias}" = "latest" ]; then
            tags="${tags} ${arch}";
          fi;
        done;

        staging_image="${STAGING_IMAGE_PREFIX%:*}:${staging_tag}";
        echo "\#\#\#\#\# To push ${staging_image} as ${tags} \#\#\#\#\#";
        if [ "${CI_COMMIT_BRANCH}" != "${CI_DEFAULT_BRANCH}" ]; then
          continue;
        fi;

        docker pull --quiet "${staging_image}";
        for variant in curl scm ''; do
          current_image="${staging_image}${variant:+-${variant}}";
          docker pull --quiet "${current_image}";
          for tag in ${tags}; do
            for prefix in ${image_prefixes}; do
              image="${prefix}${tag}${variant:+-${variant}}";
              docker tag "${current_image}" "${image}";
              docker tag "${current_image}" "${image}-${IMAGE_VERSION}";
              docker push --quiet "${image}";
              docker push --quiet "${image}-${IMAGE_VERSION}";
              docker rmi "${image}";
              docker rmi "${image}-${IMAGE_VERSION}";
            done;
          done;
          docker rmi "${current_image}";
        done;
      done
  after_script:
    - docker images

## BEGIN GENERATED JOBS ##

build:debian:bookworm:amd64:
  extends: .build-template

build:debian:bookworm:arm64:
  extends: .build-template

build:debian:bookworm:armel:
  extends: .build-template

build:debian:bookworm:armhf:
  extends: .build-template

build:debian:bookworm:i386:
  extends: .build-template

build:debian:bookworm:mips64el:
  extends: .build-template

build:debian:bookworm:mipsel:
  extends: .build-template

build:debian:bookworm:ppc64el:
  extends: .build-template

build:debian:bookworm:s390x:
  extends: .build-template

build:debian:bullseye:amd64:
  extends: .build-template

build:debian:bullseye:arm64:
  extends: .build-template

build:debian:bullseye:armel:
  extends: .build-template

build:debian:bullseye:armhf:
  extends: .build-template

build:debian:bullseye:i386:
  extends: .build-template

build:debian:bullseye:mips64el:
  extends: .build-template

build:debian:bullseye:mipsel:
  extends: .build-template

build:debian:bullseye:ppc64el:
  extends: .build-template

build:debian:bullseye:s390x:
  extends: .build-template

build:debian:buster:amd64:
  extends: .build-template

build:debian:buster:arm64:
  extends: .build-template

build:debian:buster:armel:
  extends: .build-template

build:debian:buster:armhf:
  extends: .build-template

build:debian:buster:i386:
  extends: .build-template

build:debian:buster:mips:
  extends: .build-template

build:debian:buster:mips64el:
  extends: .build-template

build:debian:buster:mipsel:
  extends: .build-template

build:debian:buster:ppc64el:
  extends: .build-template

build:debian:buster:s390x:
  extends: .build-template

build:debian:sid:alpha:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/36
  allow_failure: true

build:debian:sid:amd64:
  extends: .build-template

build:debian:sid:arm64:
  extends: .build-template

build:debian:sid:armel:
  extends: .build-template

build:debian:sid:armhf:
  extends: .build-template

build:debian:sid:hppa:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/58
  allow_failure: true

build:debian:sid:hurd-i386:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/6
  allow_failure: true

build:debian:sid:i386:
  extends: .build-template

build:debian:sid:kfreebsd-amd64:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/4
  allow_failure: true

build:debian:sid:kfreebsd-i386:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/5
  allow_failure: true

build:debian:sid:m68k:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/58
  allow_failure: true

build:debian:sid:mips64el:
  extends: .build-template

build:debian:sid:mipsel:
  extends: .build-template

build:debian:sid:powerpc:
  extends: .build-template
  # https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/58
  allow_failure: true

build:debian:sid:powerpcspe:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/37
  allow_failure: true

build:debian:sid:ppc64:
  extends: .build-template
  # https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/59
  allow_failure: true

build:debian:sid:ppc64el:
  extends: .build-template

build:debian:sid:riscv64:
  extends: .build-template

build:debian:sid:s390x:
  extends: .build-template

build:debian:sid:sh4:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/58
  allow_failure: true

build:debian:sid:sparc64:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/48
  allow_failure: true

build:debian:sid:x32:
  extends: .build-template
  # https://github.com/vicamo/docker_buildpack-deps/issues/51
  allow_failure: true

build:debian:stretch:amd64:
  extends: .build-template

build:debian:stretch:arm64:
  extends: .build-template

build:debian:stretch:armel:
  extends: .build-template

build:debian:stretch:armhf:
  extends: .build-template

build:debian:stretch:i386:
  extends: .build-template

build:debian:stretch:mips:
  extends: .build-template

build:debian:stretch:mips64el:
  extends: .build-template

build:debian:stretch:mipsel:
  extends: .build-template

build:debian:stretch:ppc64el:
  extends: .build-template

build:debian:stretch:s390x:
  extends: .build-template

build:ubuntu:bionic:amd64:
  extends: .build-template

build:ubuntu:bionic:arm64:
  extends: .build-template

build:ubuntu:bionic:armhf:
  extends: .build-template

build:ubuntu:bionic:i386:
  extends: .build-template

build:ubuntu:bionic:ppc64el:
  extends: .build-template

build:ubuntu:bionic:s390x:
  extends: .build-template

build:ubuntu:focal:amd64:
  extends: .build-template

build:ubuntu:focal:arm64:
  extends: .build-template

build:ubuntu:focal:armhf:
  extends: .build-template

build:ubuntu:focal:ppc64el:
  extends: .build-template

build:ubuntu:focal:riscv64:
  extends: .build-template
  # https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/57
  allow_failure: true

build:ubuntu:focal:s390x:
  extends: .build-template

build:ubuntu:hirsute:amd64:
  extends: .build-template

build:ubuntu:hirsute:arm64:
  extends: .build-template

build:ubuntu:hirsute:armhf:
  extends: .build-template

build:ubuntu:hirsute:ppc64el:
  extends: .build-template

build:ubuntu:hirsute:riscv64:
  extends: .build-template

build:ubuntu:hirsute:s390x:
  extends: .build-template

build:ubuntu:impish:amd64:
  extends: .build-template

build:ubuntu:impish:arm64:
  extends: .build-template

build:ubuntu:impish:armhf:
  extends: .build-template

build:ubuntu:impish:ppc64el:
  extends: .build-template

build:ubuntu:impish:riscv64:
  extends: .build-template

build:ubuntu:impish:s390x:
  extends: .build-template

build:ubuntu:jammy:amd64:
  extends: .build-template

build:ubuntu:jammy:arm64:
  extends: .build-template

build:ubuntu:jammy:armhf:
  extends: .build-template

build:ubuntu:jammy:ppc64el:
  extends: .build-template

build:ubuntu:jammy:riscv64:
  extends: .build-template

build:ubuntu:jammy:s390x:
  extends: .build-template

build:ubuntu:kinetic:amd64:
  extends: .build-template

build:ubuntu:kinetic:arm64:
  extends: .build-template

build:ubuntu:kinetic:armhf:
  extends: .build-template

build:ubuntu:kinetic:ppc64el:
  extends: .build-template

build:ubuntu:kinetic:riscv64:
  extends: .build-template

build:ubuntu:kinetic:s390x:
  extends: .build-template

build:ubuntu:xenial:amd64:
  extends: .build-template

build:ubuntu:xenial:arm64:
  extends: .build-template
  variables:
    JOB_QEMU_SUITE: 'bionic'

build:ubuntu:xenial:armhf:
  extends: .build-template
  variables:
    JOB_QEMU_SUITE: 'bionic'

build:ubuntu:xenial:i386:
  extends: .build-template

build:ubuntu:xenial:ppc64el:
  extends: .build-template
  variables:
    JOB_QEMU_SUITE: 'bionic'

build:ubuntu:xenial:s390x:
  extends: .build-template
  variables:
    JOB_QEMU_SUITE: 'bionic'
  # https://github.com/vicamo/docker-brew-ubuntu-debootstrap/issues/2
  allow_failure: true

deploy:bionic:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:bookworm:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:bullseye:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:buster:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:focal:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:hirsute:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:impish:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:jammy:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:kinetic:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:sid:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:stretch:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:xenial:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

## END GENERATED JOBS ##

test:deploy:
  extends: .deploy-template
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

cleanup:
  stage: cleanup
  image: docker
  services:
    - docker:dind
  dependencies: []
  variables:
    GIT_STRATEGY: none
  before_script:
    - *retrieve-reg-tool
  script:
    - staging_tags=$(
        /usr/local/bin/reg tags --auth-url "${CI_REGISTRY}"
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
            "${STAGING_IMAGE_PREFIX%:*}" |
          grep "^${STAGING_IMAGE_PREFIX##*:}" || true)
    - test -z "${staging_tags}" || for staging_tag in ${staging_tags}; do
        /usr/local/bin/reg rm -d --auth-url "${CI_REGISTRY}"
            -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
            "${STAGING_IMAGE_PREFIX%:*}:${staging_tag}" || true;
      done
  rules:
    - when: always
